pub use super::*;

/// A command to the `isotope` rewpl
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Command {
    /// An `isotope` expression
    Expr(Expr),
    /// An `isotope` statement
    Stmt(Stmt),
    /// Get the code of an expression
    Code(Expr),
    /// Get the ID of an expression
    Id(Expr),
    /// Get the address of an expression
    Addr(Expr),
    /// Get the flags of an expression
    Flags(Expr),
    /// Typecheck an expression
    Tyck(Expr),
}

/// Parse a command
pub fn command(input: &str) -> IResult<&str, Command> {
    alt((
        map(expr, Command::Expr),
        map(stmt, Command::Stmt),
        map(preceded(preceded(tag("#code"), ws), expr), Command::Code),
        map(preceded(preceded(tag("#id"), ws), expr), Command::Id),
        map(preceded(preceded(tag("#addr"), ws), expr), Command::Addr),
        map(preceded(preceded(tag("#flags"), ws), expr), Command::Flags),
        map(preceded(preceded(tag("#tyck"), ws), expr), Command::Tyck),
    ))(input)
}
