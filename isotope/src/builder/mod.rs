/*!
Convert `isotope` ASTs to terms
*/
use crate::*;
use ast::{Expr, Join, Let, Stmt};
use hayami::{SymbolMap, SymbolTable};

/// A converter for `isotope` ASTs into in-memory representation
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Builder<T = StandardCtx> {
    /// The current symbol table
    symbols: SymbolTable<String, TableEntry, ahash::RandomState>,
    /// The current set of variable types
    vars: Vec<Option<TermId>>,
    /// The default number of join steps
    default_join_steps: u64,
    /// This builder's underlying typing context
    ctx: T,
}

impl<T: Default + TyCtxMut> Default for Builder<T> {
    fn default() -> Self {
        Builder {
            symbols: SymbolTable::default(),
            vars: Vec::new(),
            default_join_steps: 200,
            ctx: T::default(),
        }
    }
}

/// An entry in a symbol table
#[derive(Debug, Clone, PartialEq, Eq)]
struct TableEntry {
    /// The substitution
    term: TermId,
    /// The level of this substitution
    level: u32,
}

impl TableEntry {
    /// Get an entry at a given level
    fn get(&self, level: u32, ctx: &mut (impl ConsCtx + ?Sized)) -> TermId {
        let shift = level - self.level;
        //TODO: error fixing, overflow...
        self.term.shifted(shift as i32, 0, ctx).unwrap()
    }
}

impl<T, M> Builder<T>
where
    T: TyCtxMut<TermEqCtx = M>,
    M: TermEqCtxEdit,
{
    /// Create a new builder for `isotope` values
    #[inline]
    pub fn new(ctx: T) -> Builder<T> {
        Builder {
            symbols: SymbolTable::default(),
            vars: Vec::new(),
            default_join_steps: 200,
            ctx,
        }
    }

    /// Handle a statement
    #[inline]
    pub fn stmt(&mut self, stmt: &Stmt) -> Result<(), Error> {
        match stmt {
            Stmt::Let(stmt) => self.let_(stmt),
            Stmt::Join(stmt) => self.join(stmt),
        }
    }

    /// Handle a let-statement
    #[inline]
    fn let_(&mut self, stmt: &Let) -> Result<(), Error> {
        let value = if let Some(ty) = &stmt.ty {
            self.expr_with(&stmt.value, ty)?
        } else {
            self.expr(&stmt.value)?
        };
        if let Some(ident) = &stmt.ident {
            self.register_subst(ident.clone(), value);
        }
        Ok(())
    }

    /// Handle a join statement
    #[inline]
    fn join(&mut self, stmt: &Join) -> Result<(), Error> {
        let source = self.expr(&stmt.source)?;
        let target = stmt
            .target
            .as_deref()
            .map(|target| self.expr(target))
            .transpose()?;
        if let Some(target) = &target {
            match source.eq_in(target, self.ctx.eq_ctx()) {
                Some(true) => return Ok(()),
                Some(false) => return Err(Error::TermMismatch),
                None if stmt.form == ast::Form::Null => return Err(Error::TermUnificationFailure),
                None => {}
            }
        }
        let max_steps = stmt.work_limit.unwrap_or(self.default_join_steps);
        let cfg = NormalCfg {
            max_steps,
            eta: stmt.form.eta(),
            head: stmt.form.head(),
            sub: stmt.form.sub(),
        };
        let redex = source.reduce_until(cfg, target.clone(), &mut self.ctx)?;
        if let Some(target) = &target {
            if let Some(redex) = redex {
                match redex.eq_in(target, self.ctx.eq_ctx()) {
                    Some(true) => {}
                    Some(false) => return Err(Error::TermMismatch),
                    None => return Err(Error::TermUnificationFailure),
                }
                self.ctx.eq_ctx().shallow_cons_make_term_eq(&source, &redex);
            } else {
                // No change was made, and equality was already checked beforehand
                return Err(Error::TermUnificationFailure);
            }
        }
        Ok(())
    }

    /// Build an expression into a term
    #[inline]
    pub fn expr(&mut self, expr: &Expr) -> Result<TermId, Error> {
        let result = match expr {
            Expr::Ident(ident) => self.ident(&ident[..])?,
            Expr::Var(ix) => self.var(*ix),
            Expr::App(app) => self.app(app)?,

            Expr::Lambda(lambda) => self.lambda(lambda)?.into_id_with(self.ctx.cons_ctx()),
            Expr::Pi(pi) => self.pi(pi)?.into_id_with(self.ctx.cons_ctx()),
            Expr::Universe(universe) => self.universe(*universe)?.into_id_with(self.ctx.cons_ctx()),

            Expr::Enum(enum_) => self.enum_(enum_)?.into_id_with(self.ctx.cons_ctx()),
            Expr::Variant(variant) => self.variant(variant)?.into_id_with(self.ctx.cons_ctx()),
            Expr::Bool => self.get_bool().into_id_with(self.ctx.cons_ctx()),
            Expr::Boolean(boolean) => self.boolean(*boolean).into_id_with(self.ctx.cons_ctx()),

            Expr::Case(case) => self.case(case)?.into_id_with(self.ctx.cons_ctx()),

            Expr::Annotated(annotated) => self.annotated(annotated)?,
            Expr::Scope(scope) => self.scope(scope)?,

            _ => return Err(Error::NotImplemented),
        };
        Ok(result)
    }

    /// Get a variable
    #[inline]
    pub fn get_var(&mut self, ix: u32) -> Option<Option<TermId>> {
        //TODO: overflow et al
        if (ix as usize) < self.vars.len() {
            let rev_ix = self.vars.len() - 1 - (ix as usize);
            //TODO: fix this...
            self.vars[rev_ix]
                .shifted(
                    self.vars.len() as i32 - 1 - ix as i32,
                    0,
                    self.ctx.cons_ctx(),
                )
                .ok()
        } else {
            None
        }
    }

    /// Build a variable
    #[inline]
    pub fn var(&mut self, ix: u32) -> TermId {
        let ty = self.get_var(ix);
        Var::new_unchecked(ix, ty.flatten()).into_id_with(self.ctx.cons_ctx())
    }

    /// Build an identifier
    #[inline]
    pub fn ident(&mut self, ident: &str) -> Result<TermId, Error> {
        if let Some(entry) = self.symbols.get(ident) {
            let result = entry.get(self.vars.len() as u32, self.ctx.cons_ctx());
            Ok(result)
        } else {
            Err(Error::UndefinedSymbol)
        }
    }

    /// Build an application
    #[inline]
    pub fn app(&mut self, app: &ast::App) -> Result<TermId, Error> {
        self.app_slice(&app.0[..])
    }

    /// Build an application from a slice of expressions
    #[inline]
    pub fn app_slice<E>(&mut self, slice: &[E]) -> Result<TermId, Error>
    where
        E: Borrow<Expr>,
    {
        let first = if let Some(first) = slice.get(0) {
            self.expr(first.borrow())?
        } else {
            return Err(Error::NotImplemented); // return nil
        };
        self.apply_to_slice(first, &slice[1..])
    }

    /// Apply a term to a slice of expressions
    #[inline]
    pub fn apply_to_slice<E>(&mut self, func: TermId, slice: &[E]) -> Result<TermId, Error>
    where
        E: Borrow<Expr>,
    {
        let first = if let Some(first) = slice.get(0) {
            self.expr(first.borrow())?
        } else {
            return Ok(func);
        };
        let app = self
            .construct_app(func, first)?
            .into_id_with(self.ctx.cons_ctx());
        self.apply_to_slice(app, &slice[1..])
    }

    /// Build an application within this context
    #[inline]
    pub fn construct_app(&mut self, left: TermId, right: TermId) -> Result<App, Error> {
        //TODO: typecheck, etc
        Ok(App::new_direct(left, right, None, &mut self.ctx))
    }

    /// Build a lambda function
    #[inline]
    pub fn lambda(&mut self, lambda: &ast::Lambda) -> Result<Lambda, Error> {
        let param_ty = lambda
            .param_ty
            .as_ref()
            .map(|param_ty| self.expr(param_ty))
            .transpose()?;
        let result = self.parametrized(
            param_ty.as_ref(),
            lambda.param_name.as_deref(),
            &lambda.result,
        )?;
        Ok(Lambda::new_direct(param_ty, result, self.ctx.cons_ctx()))
    }

    /// Build a pi type
    #[inline]
    pub fn pi(&mut self, pi: &ast::Pi) -> Result<Pi, Error> {
        let param_ty = self.expr(&pi.param_ty)?;
        let result = self.parametrized(
            Some(&param_ty),
            pi.param_name.as_ref().map(Option::as_deref).flatten(),
            &pi.result,
        )?;
        //TODO: typecheck?
        Ok(Pi::new(param_ty, result, self.ctx.cons_ctx()))
    }

    /// Build a parametrized term
    #[inline]
    pub fn parametrized(
        &mut self,
        param_ty: Option<&TermId>,
        param_name: Option<&str>,
        result: &ast::Expr,
    ) -> Result<TermId, Error> {
        self.vars.push(param_ty.cloned());
        let nameframe = if let Some(name) = param_name {
            let var = self.var(0);
            self.symbols.push();
            self.register_subst(name.to_string(), var);
            true
        } else {
            false
        };
        let result = self.expr(result);
        if nameframe {
            self.symbols.pop();
        }
        self.vars.pop();
        result
    }

    /// Build a typing universe
    #[inline]
    pub fn universe(&mut self, universe: ast::Universe) -> Result<Universe, Error> {
        Universe::try_new(universe.level(), universe.is_var())
    }

    /// Build an enumeration
    pub fn enum_(&mut self, enum_: &ast::Enum) -> Result<Enum, Error> {
        let set = enum_.0.iter().cloned().collect();
        Ok(Enum::new(set))
    }

    /// Build a variant
    pub fn variant(&mut self, variant: &str) -> Result<Variant, Error> {
        //TODO: improve this? reduce allocations?
        let ty = Enum::new(StringSet::singleton(variant)).into_id_with(self.ctx.cons_ctx());
        Variant::new_direct(variant, ty)
    }

    /// Build the type of booleans
    #[inline]
    pub fn get_bool(&mut self) -> Bool {
        Bool::default()
    }

    /// Build a boolean
    #[inline]
    pub fn boolean(&mut self, boolean: bool) -> Boolean {
        Boolean::new_unchecked(boolean, None)
    }

    /// Build a case statement
    #[inline]
    pub fn case(&mut self, case: &ast::Case) -> Result<Case, Error> {
        //TODO: clean this up, bigtime...
        if case.0.len() == 0 {
            return Err(Error::NotImplemented);
        }
        let target_0 = self.expr(&case.0[0].pattern)?;
        let matches = target_0
            .ty()
            .ok_or(Error::CannotInfer)?
            .clone_into_id_with(self.ctx.cons_ctx());
        let mut branches: SmallVec<[_; 16]> = case
            .0
            .iter()
            .enumerate()
            .map(|(i, branch)| {
                let result = self.expr(&branch.result)?;
                let tmp;
                let target = if i == 0 {
                    &target_0
                } else {
                    tmp = self.expr(&branch.pattern)?;
                    &tmp
                };
                //TODO: clean up this error message...
                let ix = Case::get_match(target, &matches)?.ok_or(Error::OutOfGas)?;
                Ok((ix, result))
            })
            .collect::<Result<_, _>>()?;
        branches.sort_by_key(|(ix, _)| *ix);
        let len = branches.len();
        branches.dedup_by_key(|(ix, _)| *ix);
        if branches.len() < len {
            warn!(
                "{} shadowed branches in case statement",
                len - branches.len()
            )
        }
        let cases: SmallVec<[_; 32]> = branches.into_iter().map(|(_ix, b)| b).collect();
        Case::new_minimal((&cases[..]).into(), matches, &mut self.ctx)
    }

    /// Build an annotated term
    #[inline]
    pub fn annotated(&mut self, annotated: &ast::Annotated) -> Result<TermId, Error> {
        self.expr_with(&annotated.term, &annotated.ty)
    }

    /// Build a term with an annotation
    #[inline]
    pub fn expr_with(&mut self, expr: &Expr, annot: &Expr) -> Result<TermId, Error> {
        let annot = self.expr(&annot)?;
        let term = self.expr(&expr)?;
        let coerced = term.coerce(Some(annot), &mut self.ctx)?.unwrap_or(term);
        Ok(coerced)
    }

    /// Build a scope
    #[inline]
    pub fn scope(&mut self, scope: &ast::Scope) -> Result<TermId, Error> {
        let mut errno = Ok(());
        self.ctx.reset_unbound()?;
        self.symbols.push();
        for stmt in &scope.stmts {
            if let Err(err) = self.stmt(stmt) {
                errno = Err(err);
                continue;
            }
        }
        let result = match errno {
            Ok(()) => self.expr(&scope.result),
            Err(err) => Err(err),
        };
        self.symbols.pop();
        result
    }

    /// Register a substitution
    #[inline]
    pub fn register_subst(&mut self, symbol: String, subst: TermId) {
        self.symbols.insert(
            symbol,
            TableEntry {
                term: subst,
                level: self.vars.len() as u32,
            },
        )
    }

    /// Get this builder's underlying typing context
    #[inline]
    pub fn ctx(&mut self) -> &mut T {
        &mut self.ctx
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn untyped_round_trip() {
        let examples = [
            "#true",
            "#false",
            "#bool",
            "λx.x",
            "(λx.x)(λx.x)",
            "{
                #let x = #true;
                x
            }",
            "#enum { 'x 'y 'z }",
            "#case { #true => #false; #false => #true }",
        ];

        let ctx = StandardCtx::default();
        let mut builder = Builder::new(ctx);

        for example in examples {
            let (rest, parsed) = isotope_parser::expr(example).unwrap();
            assert_eq!(rest, "");
            let built = builder.expr(&parsed).unwrap();
            let ast = built.to_ast_in(&mut Untyped).unwrap();
            //NOTE: ast may not be equal to parsed, as the builder may e.g. discard variable names and unpack scopes
            let built_ast = builder.expr(&ast).unwrap();
            //NOTE: we check for pointer equality since this builder is consing
            assert_eq!(built.as_ptr(), built_ast.as_ptr());
        }
    }

    #[test]
    fn basic_head_reduction() {
        let program = [
            ("#let i = λx:#bool.x;", true),
            ("#eq (i #true) => #true;", false),
            ("#eq (i #false) => #false;", false),
            ("#head (i #true) => #true;", true),
            ("#eq (i #true) => #true;", true),
            ("#eq (i #false) => #false;", false),
            ("#head (i #false) => #false;", true),
            ("#eq (i #true) => #true;", true),
            ("#eq (i #false) => #false;", true),
            ("#head #true => #false;", false),
            // We repeat this twice to make sure false facts are not being entered into the database
            ("#head #true => #false;", false),
            ("#head (i #true) => #false;", false),
        ];

        let ctx = StandardCtx::default();
        let mut builder = Builder::new(ctx);

        for (line, pass) in program {
            let (rest, parsed) = isotope_parser::stmt(line).unwrap();
            assert_eq!(rest, "");
            let result = builder.stmt(&parsed);
            if pass {
                assert_eq!(result, Ok(()))
            } else {
                assert!(result.is_err())
            }
        }
    }
}
