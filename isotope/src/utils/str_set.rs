/*!
A simple set of strings, for use with variants
*/
use std::iter::FromIterator;

use super::*;

/// A simple immutable set of strings for use with variants
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct StringSet {
    inner: Arc<[SmolStr]>,
}

impl<S> FromIterator<S> for StringSet
where
    S: Into<SmolStr>,
{
    fn from_iter<T: IntoIterator<Item = S>>(iter: T) -> Self {
        let mut buf: Vec<SmolStr> = iter.into_iter().map(Into::into).collect();
        buf.sort_unstable();
        buf.dedup();
        StringSet { inner: buf.into() }
    }
}

impl StringSet {
    /// Create a new singleton
    #[inline]
    pub fn singleton(s: impl Into<SmolStr>) -> StringSet {
        StringSet {
            inner: Arc::from([s.into()]),
        }
    }

    /// Get whether a string is contained by this set
    #[inline]
    pub fn contains(&self, s: &str) -> bool {
        self.inner.binary_search_by_key(&s, |x| &**x).is_ok()
    }

    /// Get the size of this set
    #[inline]
    pub fn len(&self) -> usize {
        self.inner.len()
    }

    /// Compare this set to another
    #[inline]
    pub fn set_cmp(&self, other: &StringSet) -> Option<Ordering> {
        match self.len().cmp(&other.len()) {
            Greater if other.is_subset(self) => Some(Greater),
            Equal if self == other => Some(Equal),
            Less if self.is_subset(other) => Some(Less),
            _ => None,
        }
    }

    /// Get whether this subset is a (non-strict) subset of another
    #[inline]
    pub fn is_subset(&self, other: &StringSet) -> bool {
        if self.len() > other.len() {
            return false;
        }
        let mut other = &other.inner[..];
        for s in self.iter() {
            if let Ok(ix) = other.binary_search_by_key(&s, |x| &**x) {
                other = &other[(ix + 1)..];
            } else {
                return false;
            }
        }
        true
    }

    /// Iterate over the strings in this set
    #[inline]
    pub fn iter(&self) -> impl Iterator<Item = &str> {
        self.inner.iter().map(|s| &**s)
    }
}

impl Index<usize> for StringSet {
    type Output = str;

    fn index(&self, index: usize) -> &Self::Output {
        &self.inner[index]
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn basic_string_set_properties() {
        let a: StringSet = ["x", "y", "z", "x"].iter().map(SmolStr::new).collect();
        assert_eq!(a.len(), 3);
        assert!(a.is_subset(&a));
        assert_eq!(a.set_cmp(&a), Some(Equal));
        assert!(a.contains("x"));
        assert!(!a.contains("w"));
        let b: StringSet = ["x", "y", "z", "t", "t", "z", "x"]
            .iter()
            .map(SmolStr::new)
            .collect();
        assert_eq!(b.len(), 4);
        assert_ne!(a, b);
        assert_eq!(b.set_cmp(&b), Some(Equal));
        assert_eq!(a.set_cmp(&b), Some(Less));
        assert_eq!(b.set_cmp(&a), Some(Greater));
        assert!(a.is_subset(&b));
        assert!(!b.is_subset(&a));
    }
}
