/*!
Machine representations for `isotope` values and types

To ensure that all `isotope` programs can be compiled to efficient machine code,
every value is annotated with both a type and a *representation*: the actual
layout (pre-optimization) of that value in memory. While the types of local
variables may be dependent on runtime parameters, representations *must* only
depend on compile-time constants (i.e., generic parameters).
*/

/// A machine representation for `isotope` values and types
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum Repr {}
